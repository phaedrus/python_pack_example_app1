#!/usr/bin/env python

from setuptools import find_packages
from setuptools import setup


setup(name='App1',
      version='1.0',
      description='Testing python dependencies',
      packages=find_packages(),
      install_requires=['lib1>=0.1.0'],
      dependency_links=[
          'git+https://phaedrus@bitbucket.org/phaedrus/python_pack_lib1.git#egg=lib1-0.1.0',  # noqa
        ],
      entry_points={
            'console_scripts': ['app1=app1.app:use_foo']
              }
      )
