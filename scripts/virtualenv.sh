#!/bin/bash
#
# Usage: `source <path to this script>.sh`
#
# This activates the virtual environment, like:
# ```
# (.venv) $
# ```
# To exit the virtual environment, use `deactivate`:
# ```
# (.venv) $ deactivate
# ```

source .venv/bin/activate

# For all 
export PYTHONPATH=$PYTHONPATH:.
