from lib1.lib1mod1 import foo


def use_foo():
    print("Got foo=[%s] from lib1" % foo)


if __name__ == '__main__':
    use_foo()
